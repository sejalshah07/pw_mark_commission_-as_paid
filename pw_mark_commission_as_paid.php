<?php
/*
Plugin Name: Mark commission as Paid or Unpaid
Plugin URI: photowhoa.com
Description: Mark selected commission as Paid or Unpaid
Author: Sejal Shah
Author URI: 
Version: 0.1
*/

if (!class_exists('PW_Custom_Bulk_Action')) {
 
	class PW_Custom_Bulk_Action {
		
		public function __construct() {
			
			if(is_admin()) {
				// admin actions/filters
				add_action('admin_footer-edit.php', array(&$this, 'custom_bulk_admin_footer'));
				add_action('load-edit.php',         array(&$this, 'custom_bulk_action_mark_as_paid'));
				add_action('load-edit.php',         array(&$this, 'custom_bulk_action_mark_as_unpaid'));
				add_action('admin_notices',         array(&$this, 'custom_bulk_admin_notices'));
			}
		}
		
		
		/**
		 * Step 1: add the custom Bulk Action 'mark as paid' and 'mark as unpaid' to the select menus
		 */
		function custom_bulk_admin_footer() {
			global $post_type;
			
			if($post_type == 'shop_commission') {
				?>
					<script type="text/javascript">
						jQuery(document).ready(function() {
							jQuery('<option>').val('mark_selected_as_paid').text('<?php _e('Mark selected commission as paid')?>').appendTo("select[name='action']");
							jQuery('<option>').val('mark_selected_as_paid').text('<?php _e('Mark selected commission as paid')?>').appendTo("select[name='action2']");
							jQuery('<option>').val('mark_selected_as_unpaid').text('<?php _e('Mark selected commission as unpaid')?>').appendTo("select[name='action']");
							jQuery('<option>').val('mark_selected_as_unpaid').text('<?php _e('Mark selected commission as unpaid')?>').appendTo("select[name='action2']");
						});
					</script>
				<?php
	    	}
		}
		
		
		/**
		 * Step 2: Mark Commission as Paid
		 */
		function custom_bulk_action_mark_as_paid() {
			global $typenow;
			$post_type = $typenow;
			
			if($post_type == 'shop_commission') {
				
				// get the action
				$wp_list_table = _get_list_table('WP_Posts_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
				$action = $wp_list_table->current_action();
				
				$allowed_actions = array("mark_selected_as_paid");
				if(!in_array($action, $allowed_actions)) return;
				
				// security check
				check_admin_referer('bulk-posts');
				
				// make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
				if(isset($_REQUEST['post'])) {
					$post_ids = array_map('intval', $_REQUEST['post']);
				}
				
				if(empty($post_ids)) return;
				
				// this is based on wp-admin/edit.php
				$sendback = remove_query_arg( array('marked', 'untrashed', 'deleted', 'ids'), wp_get_referer() );
				if ( ! $sendback )
					$sendback = admin_url( "edit.php?post_type=$post_type" );
				
				$pagenum = $wp_list_table->get_pagenum();
				$sendback = add_query_arg( 'paged', $pagenum, $sendback );
				
				switch($action) {
					case 'mark_selected_as_paid':
						$marked = 0;
						foreach( $post_ids as $post_id ) {
							
							if ( !$this->perform_mark_selected_as_paid($post_id) )
								wp_die( __('Error marking commission.') );
			
							$marked++;
						}
						
						$sendback = add_query_arg( array('marked' => $marked, 'ids' => join(',', $post_ids) ), $sendback );
					break;
					
					default: return;
				}
				
				$sendback = remove_query_arg( array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status',  'post', 'bulk_edit', 'post_view'), $sendback );
				
				wp_redirect($sendback);
				exit();
			}
		}
		
		/**
		 * Step 2: Mark Commission as Unpaid
		 */
		function custom_bulk_action_mark_as_unpaid() {
			global $typenow;
			$post_type = $typenow;
			
			if($post_type == 'shop_commission') {
				
				// get the action
				$wp_list_table = _get_list_table('WP_Posts_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
				$action = $wp_list_table->current_action();
				
				$allowed_actions = array("mark_selected_as_unpaid");
				if(!in_array($action, $allowed_actions)) return;
				
				// security check
				check_admin_referer('bulk-posts');
				
				// make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
				if(isset($_REQUEST['post'])) {
					$post_ids = array_map('intval', $_REQUEST['post']);
				}
				
				if(empty($post_ids)) return;
				
				// this is based on wp-admin/edit.php
				$sendback = remove_query_arg( array('marked', 'untrashed', 'deleted', 'ids'), wp_get_referer() );
				if ( ! $sendback )
					$sendback = admin_url( "edit.php?post_type=$post_type" );
				
				$pagenum = $wp_list_table->get_pagenum();
				$sendback = add_query_arg( 'paged', $pagenum, $sendback );
				
				switch($action) {
					case 'mark_selected_as_unpaid':
						
						$marked = 0;
						foreach( $post_ids as $post_id ) {
							
							if ( !$this->perform_mark_selected_as_unpaid($post_id) )
								wp_die( __('Error marking commission.') );
			
							$marked++;
						}
						
						$sendback = add_query_arg( array('marked' => $marked, 'ids' => join(',', $post_ids) ), $sendback );
					break;
					
					default: return;
				}
				
				$sendback = remove_query_arg( array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status',  'post', 'bulk_edit', 'post_view'), $sendback );
				
				wp_redirect($sendback);
				exit();
			}
		}
		/*
		* Perform Marking 'paid'
		*/
		function perform_mark_selected_as_paid($post_id) {
			update_post_meta( $post_id, '_paid_status', 'paid');
			return true;
		}
		
		/*
		* Perform Marking 'unpaid'
		*/
		function perform_mark_selected_as_unpaid($post_id) {
			update_post_meta( $post_id, '_paid_status', 'unpaid');
			return true;
		}
		
		
		/**
		 * Step 3: display an admin notice on the Posts page after marking
		 */
		function custom_bulk_admin_notices() {
			global $post_type, $pagenow;
			
			if($pagenow == 'edit.php' && $post_type == 'post' && isset($_REQUEST['marked']) && (int) $_REQUEST['marked']) {
				$message = sprintf( _n( 'Commission marked.', '%s commission marked.', $_REQUEST['marked'] ), number_format_i18n( $_REQUEST['marked'] ) );
				echo "<div class=\"updated\"><p>{$message}</p></div>";
			}
		}
		
	}
}

new PW_Custom_Bulk_Action();
