=== PW Mark Selected Commission as Paid or Unpaid ===
Contributors: Sejal Shah
Requires at least: 3.3
Tested up to: 3.3
Stable tag: 0.1

Provides User Interface for marking commission as paid or unpaid

== Description ==
Provides User Interface for marking commission as paid or unpaid

== Installation ==

1. Upload the entire 'pw_mark_commission_as_paid' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 0.1 =
* Initial release
